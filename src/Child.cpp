#include "CChild.h"
#include <iostream>
#include <csignal>

void signalHandler( int signum ) {
    std::cout << "Child received signal (" << signum << ").\n"; 
    CChild& child = CChild::getInstance();
    child.Stop();   
}

int main(int argc, char *argv[])
{
    
    const std::uint32_t DEFAULT_PERIOD = 1000;
    std::uint32_t period = DEFAULT_PERIOD;
    if (argc > 1)
    {
        try
        {
            period = std::stoi(argv[1]);
            if(period > DEFAULT_PERIOD || period < 1)
            {
                period = DEFAULT_PERIOD;
                std::cout << "Child: Incorrect period. Continue with default = " << DEFAULT_PERIOD << "ms" << std::endl;
            }
            
        }
        catch (const std::exception& e) 
        {
            std::cerr << "Child exception:" << e.what() << std::endl; 
            std::cerr << "Child: Continue with default preiod " << DEFAULT_PERIOD << "ms" << std::endl;
        }
    }

    try
    {
        CChild& child = CChild::getInstance();
        child.SetPeriod(period);
        signal(SIGINT, signalHandler);
        signal(SIGTERM, signalHandler);
        signal(SIGABRT, signalHandler);
        child.Run();
        std::cout << "Child stopped" << std::endl;
    }
    catch(std::exception &e)
    {
        std::cerr << "Child exception: " << e.what() << '\n';
    }
    catch (...){
        std::cerr << "Child - unexpected exception" << std::endl;
    }
    
    return 0;
}