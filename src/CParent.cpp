#include <CParent.h> // header in local directory
#include <iostream>
#include <unistd.h>
#include <sys/wait.h>

CParent::CParent()
{
    std::cout << "Parent initialized" << std::endl;
}

CParent::~CParent()
{
    std::cout << "Parent destroyed" << std::endl;
}

bool CParent::StopRequested()
{
    return stopRequested;
}

void CParent::Stop()
{
    stopRequested = true;
}

void CParent::Run(std::string period)
{
    while (!StopRequested())
    {
        pid_t pid = fork();

        if (pid == -1)
        {
            std::cout << "Parent fork failed" << std::endl;
        } 
        else if (pid > 0)
        {
            std::cout << "Parent forked Child" << std::endl;
            std::int32_t status;
            waitpid(pid, &status, 0);
        }
        else 
        {
            std::cout << "Launching child ..." << std::endl;
            execl("Child", "", period.c_str(), NULL);
            Stop();
        }
    }
}

