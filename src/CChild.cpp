
#include <CChild.h>
#include <iostream>
#include <chrono>
#include <thread>
#include <fstream>
#include <unistd.h>

CChild::CChild() :  period {1000}, counter {0}, counterFileName{"counter.txt"}, stopRequested{false}
{
    std::cout << "Child initialized" << std::endl;
    const std::lock_guard<std::mutex> lock(counterMutex);
    std::fstream counterFile(counterFileName, std::ios::in);
    if (counterFile)
        counterFile >> counter;
    else
        std::cout << "No counter file found. Starting from 0." << std::endl;
}

CChild::~CChild()
{
    const std::lock_guard<std::mutex> lock(counterMutex);
    std::cout << "Child destroyed" << std::endl;
}

void CChild::SetPeriod(std::uint32_t initPeriod)
{
    period = initPeriod;
}

void CChild::Run()
{
    std::thread printer(&CChild::prtCounter, this);
    while(!StopRequested()) 
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(period));
        incCounter();
    }
    printer.join();
}

bool CChild::StopRequested()
{
    return stopRequested;
}

void CChild::Stop()
{
    stopRequested = true;
}

void CChild::incCounter()
{
    const std::lock_guard<std::mutex> lock(counterMutex);
    counter++;
    try
    {
        std::fstream counterFile(counterFileName, std::ios::out);
        if (counterFile)
            counterFile << counter;
        else
            std::cerr << "Error writing to counter file." << std::endl;
    }
    catch(std::exception &e)
    {
        std::cerr << "Child exception while incrementing counter:" << e.what() << " Stopping ... "<< std::endl;
        Stop();
    }
    catch(...)
    {
        std::cerr << "Child unknown error while incrementing counter. Stopping ... "<< std::endl;
        Stop();
    }
}

std::uint64_t CChild::getCounter()
{
    const std::lock_guard<std::mutex> lock(counterMutex);
    return counter;
}

void CChild::prtCounter()
{
    try
    {
        std::cout << "Child started" << std::endl;
        while(!StopRequested()) 
        {
            std::cout << "Child counter = " << getCounter() << std::endl;
            std::this_thread::sleep_for(std::chrono::milliseconds(1000));
        }
        std::cout << "Child counter printer stopped" << std::endl;
    }
    catch(std::exception &e)
    {
        std::cerr << "Child counter printer - exception:" << e.what() << " Stopping ... "<< std::endl;
        Stop();
    }
    catch(...)
    {
        std::cerr << "Child counter printer - unknown error. Stopping ... "<< std::endl;
        Stop();
    }
}
