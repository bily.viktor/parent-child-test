#include "CParent.h"
#include <iostream>
#include <csignal>

void signalHandler( int signum ) {
   std::cout << "Parent received signal (" << signum << ").\n";  
   CParent& parent = CParent::getInstance();
   parent.Stop();   
}

int main(int argc, char *argv[])
{
    try
    {
        CParent& parent = CParent::getInstance();
        signal(SIGINT, signalHandler);
        signal(SIGTERM, signalHandler);
        signal(SIGABRT, signalHandler);
        std::string period = argc > 1 ? argv[1] : "1000";
        parent.Run(period);
    }
    catch(std::exception &e)
    {
        std::cerr << "Parent exception: " << e.what() << '\n';
    }
    catch (...){
        std::cerr << "Parent - unexpected exception" << std::endl;
    }
    std::cout << "Parent stopped" << std::endl;
    return 0;
}