#include<string>
#include <atomic>

class CParent
{
    public:
        static CParent& getInstance()
        {
            static CParent instance; 
            return instance;
        }
        CParent(CParent const&)         = delete;
        void operator=(CParent const&)  = delete;
        void Run(std::string);
        void Stop();
        bool StopRequested();
        ~CParent();
    private:
        std::atomic<bool> stopRequested;
        CParent();
};