#include <mutex>
#include <string>
#include <atomic>

class CChild
{
    public:
        static CChild& getInstance()
        {
            static CChild instance; 
            return instance;
        }
        CChild(CChild const&)          = delete;
        void operator=(CChild const&)  = delete;
        void SetPeriod(std::uint32_t initPeriod);
        void Run();
        void Stop();
        bool StopRequested();
        ~CChild();
    private:
        std::uint32_t period;
        std::uint64_t counter;
        std::mutex counterMutex;
        std::string counterFileName;
        CChild();
        std::atomic<bool> stopRequested;
        void prtCounter();
        std::uint64_t getCounter();
        void incCounter();
};