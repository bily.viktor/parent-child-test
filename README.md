# Test task

This repository contains all necessary files to build 2 test application: Parent and Child

## Build from source

To build the project:  
    cd parent-child-test/build  
    cmake ..  
    make  

## Parent

Parent can be started from within the **build** directory:  
    ./Parent *period_in_ms*  

where *period_in_ms* - counter period, ms.

To save apps output to file use:  
    ./Parent period_in_ms > log.txt  

If command line argument *period_in_ms* is omitted, Child counter will be incremented with default period = 1s.
As per the assignment, Parent launches Child and restarts if it crashed or killed.  

Parent catches SIGINT, SIGTERM, SIGABRT signals for proper cleanup (even though there is no resource allocation currently).

## Child

Child is normally started by the Parent. Period for counter increment passed as argument.
On startup Child reads last known counter value from counter.txt (or set's to 0 if file is unavailable).
After each counter increment it's value saved to disk.
Child uses separate worker thread to print counter value every 1s.
Child catches SIGINT, SIGTERM, SIGABRT signals for proper cleanup.

